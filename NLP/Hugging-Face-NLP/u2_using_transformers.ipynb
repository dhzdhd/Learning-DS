{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## About transformers library\n",
    "\n",
    "The library’s main features are:\n",
    "\n",
    "- Ease of use: Downloading, loading, and using a state-of-the-art NLP model for inference can be done in just two lines of code.\n",
    "- Flexibility: At their core, all models are simple PyTorch nn.Module or TensorFlow tf.keras.Model classes and can be handled like any other models in their respective machine learning (ML) frameworks.\n",
    "- Simplicity: Hardly any abstractions are made across the library. The “All in one file” is a core concept: a model’s forward pass is entirely defined in a single file, so that the code itself is understandable and hackable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Process\n",
    "\n",
    "### Preprocessing\n",
    "\n",
    "Transformer models use **tokenizers** to convert text inputs into numerical values which the model can make sense of. The tokenizer does the following\n",
    "- Splitting the input into words, subwords, or symbols (like punctuation) that are called tokens\n",
    "- Mapping each token to an integer\n",
    "- Adding additional inputs that may be useful to the model\n",
    "\n",
    "Once we have the tokenizer, we can directly pass our sentences to it and we’ll get back a dictionary that’s ready to feed to our model\n",
    "\n",
    "Transformer models only accept tensors as input and hence we pass the `return_tensors` argument.\n",
    "\n",
    "![](https://huggingface.co/datasets/huggingface-course/documentation-images/resolve/main/en/chapter2/full_nlp_pipeline-dark.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output is a dictionary containing two keys, `input_ids` and `attention_mask`. `input_ids` contains two rows of integers that are the unique identifiers of the tokens in each sentence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'input_ids': <tf.Tensor: shape=(2, 16), dtype=int32, numpy=\n",
      "array([[  101,  1045,  1005,  2310,  2042,  3403,  2005,  1037, 17662,\n",
      "        12172,  2607,  2026,  2878,  2166,  1012,   102],\n",
      "       [  101,  1045,  5223,  2023,  2061,  2172,   999,   102,     0,\n",
      "            0,     0,     0,     0,     0,     0,     0]], dtype=int32)>, 'attention_mask': <tf.Tensor: shape=(2, 16), dtype=int32, numpy=\n",
      "array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],\n",
      "       [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]], dtype=int32)>}\n"
     ]
    }
   ],
   "source": [
    "# Preprocessing with a tokenizer\n",
    "\n",
    "# AutoTokenizer is a tokenizer; from_pretrained downloads model tokenizer data & ckpts\n",
    "from transformers import AutoTokenizer\n",
    "\n",
    "checkpoint = \"distilbert-base-uncased-finetuned-sst-2-english\"\n",
    "tokenizer = AutoTokenizer.from_pretrained(checkpoint)\n",
    "\n",
    "raw_inputs = [\n",
    "    \"I've been waiting for a HuggingFace course my whole life.\",\n",
    "    \"I hate this so much!\",\n",
    "]\n",
    "inputs = tokenizer(raw_inputs, padding=True, truncation=True, return_tensors=\"tf\")\n",
    "print(inputs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This architecture contains only the base Transformer module: given some inputs, it outputs what we’ll call hidden states, also known as features. For each model input, we’ll retrieve a high-dimensional vector representing the contextual understanding of that input by the Transformer model.\n",
    "\n",
    "While these hidden states can be useful on their own, they’re usually inputs to another part of the model, known as the head"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Some weights of the PyTorch model were not used when initializing the TF 2.0 model TFDistilBertModel: ['classifier.weight', 'classifier.bias', 'pre_classifier.bias', 'pre_classifier.weight']\n",
      "- This IS expected if you are initializing TFDistilBertModel from a PyTorch model trained on another task or with another architecture (e.g. initializing a TFBertForSequenceClassification model from a BertForPreTraining model).\n",
      "- This IS NOT expected if you are initializing TFDistilBertModel from a PyTorch model that you expect to be exactly identical (e.g. initializing a TFBertForSequenceClassification model from a BertForSequenceClassification model).\n",
      "All the weights of TFDistilBertModel were initialized from the PyTorch model.\n",
      "If your task is similar to the task the model of the checkpoint was trained on, you can already use TFDistilBertModel for predictions without further training.\n"
     ]
    }
   ],
   "source": [
    "from transformers import TFAutoModel\n",
    "\n",
    "checkpoint = \"distilbert-base-uncased-finetuned-sst-2-english\"\n",
    "model = TFAutoModel.from_pretrained(checkpoint)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The vector output by the Transformer module is usually large. They behave like namedtyples. It generally has three dimensions:\n",
    "\n",
    "- Batch size: The number of sequences processed at a time (2 in our example).\n",
    "- Sequence length: The length of the numerical representation of the sequence (16 in our example).\n",
    "- Hidden size: The vector dimension of each model input. Reason for high dimensionality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(2, 16, 768)\n"
     ]
    }
   ],
   "source": [
    "outputs = model(inputs)\n",
    "print(outputs.last_hidden_state.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model heads take the high-dimensional vector of hidden states as input and project them onto a different dimension. They are usually composed of one or a few linear layers\n",
    "\n",
    "The embeddings layer converts each input ID in the tokenized input into a vector that represents the associated token. The subsequent layers manipulate those vectors using the attention mechanism to produce the final representation of the sentences.\n",
    "\n",
    "![](https://huggingface.co/datasets/huggingface-course/documentation-images/resolve/main/en/chapter2/transformer_and_head-dark.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For our example, we will need a model with a sequence classification head (to be able to classify the sentences as positive or negative). So, we won’t actually use the TFAutoModel class, but TFAutoModelForSequenceClassification\n",
    "\n",
    "The model head takes as input the high-dimensional vectors we saw before, and outputs vectors containing two values.\n",
    "\n",
    "Since we have just two sentences and two labels, the result we get from our model is of shape 2 x 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "All PyTorch model weights were used when initializing TFDistilBertForSequenceClassification.\n",
      "\n",
      "All the weights of TFDistilBertForSequenceClassification were initialized from the PyTorch model.\n",
      "If your task is similar to the task the model of the checkpoint was trained on, you can already use TFDistilBertForSequenceClassification for predictions without further training.\n"
     ]
    }
   ],
   "source": [
    "from transformers import TFAutoModelForSequenceClassification\n",
    "\n",
    "checkpoint = \"distilbert-base-uncased-finetuned-sst-2-english\"\n",
    "model = TFAutoModelForSequenceClassification.from_pretrained(checkpoint)\n",
    "outputs = model(inputs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(2, 2)\n"
     ]
    }
   ],
   "source": [
    "print(outputs.logits.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preprocessing output\n",
    "\n",
    "The output are not probabilities but logits, the raw, unnormalized scores outputted by the last layer of the model. To be converted to probabilities, they need to go through a `SoftMax` layer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor(\n",
      "[[-1.5606971  1.612282 ]\n",
      " [ 4.1692314 -3.3464477]], shape=(2, 2), dtype=float32)\n"
     ]
    }
   ],
   "source": [
    "print(outputs.logits)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tf.Tensor(\n",
      "[[4.0195324e-02 9.5980465e-01]\n",
      " [9.9945587e-01 5.4418371e-04]], shape=(2, 2), dtype=float32)\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "predictions = tf.math.softmax(outputs.logits, axis=-1)\n",
    "print(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{0: 'NEGATIVE', 1: 'POSITIVE'}"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model.config.id2label"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can conclude that the model predicted the following:\n",
    "\n",
    "- First sentence: NEGATIVE: 0.0401, POSITIVE: 0.9598\n",
    "- Second sentence: NEGATIVE: 0.9994, POSITIVE: 0.0005"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "hfnlp",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
